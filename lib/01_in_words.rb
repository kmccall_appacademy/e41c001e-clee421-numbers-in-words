class Fixnum
  def in_words
    n = self
    if n < 100
      less_than_hundred
    elsif n < 1000
      less_than_thousand
    elsif n < 1000000
      less_than_million
    elsif n < 1000000000
      less_than_billion
    elsif n < 1000000000000
      less_than_trillion
    elsif n <1000000000000000
      less_than_quadrillion
    end
  end #in_words

  def less_than_quadrillion
    res = []
    trillion = self/1000000000000
    trillion < 100 ? res << trillion.less_than_hundred : res << trillion.less_than_thousand
    res << 'trillion'
    remainder = self%1000000000000
    if remainder < 100
      res << remainder.less_than_hundred if remainder != 0
    elsif remainder < 1000
      res << remainder.less_than_thousand if remainder != 0
    elsif remainder < 1000000
      res << remainder.less_than_million if remainder != 0
    elsif remainder < 1000000000
      res << remainder.less_than_billion if remainder != 0
    elsif remainder < 1000000000000
      res << remainder.less_than_trillion if remainder != 0
    end
    res.join(' ')
  end #less_than_quadrillion

  def less_than_trillion
    res = []
    billion = self/1000000000
    billion < 100 ? res << billion.less_than_hundred : res << billion.less_than_thousand
    res << 'billion'
    remainder = self%1000000000
    if remainder < 100
      res << remainder.less_than_hundred if remainder != 0
    elsif remainder < 1000
      res << remainder.less_than_thousand if remainder != 0
    elsif remainder < 1000000
      res << remainder.less_than_million if remainder != 0
    elsif remainder < 1000000000
      res << remainder.less_than_billion if remainder != 0
    end
    res.join(' ')
  end #less_than_trillion

  def less_than_billion
    res = []
    million = self/1000000
    million < 100 ? res << million.less_than_hundred : res << million.less_than_thousand
    res << 'million'
    remainder = self%1000000
    if remainder < 100
      res << remainder.less_than_hundred if remainder != 0
    elsif remainder < 1000
      res << remainder.less_than_thousand if remainder != 0
    elsif remainder < 1000000
      res << remainder.less_than_million if remainder != 0
    end
    res.join(' ')
  end #less_than_billion

  def less_than_million
    res = []
    thousand = self/1000
    thousand < 100 ? res << thousand.less_than_hundred : res << thousand.less_than_thousand
    res << 'thousand'
    remainder = self%1000
    if remainder < 100
      res << remainder.less_than_hundred if remainder != 0
    elsif remainder < 1000
      res << remainder.less_than_thousand if remainder != 0
    end
    res.join(' ')
  end #less_than_million

  def less_than_thousand
    res = []
    hundred = self/100
    res << hundred.less_than_ten << 'hundred'
    res << (self%100).less_than_hundred if self%100 != 0
    res.join(' ')
  end #less_than_thousand

  def less_than_hundred
    res = []
    if self <= 9
      res << less_than_ten
    elsif self <= 12
      res << prior_teen
    elsif self <= 19
      res << teens
    else
      res << tens
      res << less_than_ten unless self%10 == 0
    end
    res.join(' ')
  end #less_than_hundred

  def less_than_ten
    n = self%10
    case n
    when 0
      "zero"
    when 1
      "one"
    when 2
      "two"
    when 3
      "three"
    when 4
      "four"
    when 5
      "five"
    when 6
      "six"
    when 7
      "seven"
    when 8
      "eight"
    when 9
      "nine"
    end
  end #less_than_ten

  def prior_teen
    case self
    when 10
      "ten"
    when 11
      "eleven"
    when 12
      "twelve"
    end
  end #prior_teen

  def teens
    case self
    when 13
      "thirteen"
    when 14
      "fourteen"
    when 15
      "fifteen"
    when 16
      "sixteen"
    when 17
      "seventeen"
    when 18
      "eighteen"
    when 19
      "nineteen"
    end
  end #teens

  def tens
    n = self/10*10
    case n
    when 20
      "twenty"
    when 30
      "thirty"
    when 40
      "forty"
    when 50
      "fifty"
    when 60
      "sixty"
    when 70
      "seventy"
    when 80
      "eighty"
    when 90
      "ninety"
    end
  end #tens
end #class Fixnum
